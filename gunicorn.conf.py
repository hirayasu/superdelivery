bind = 'unix:/var/run/superdelivery.sock'
workers = 2
worker_connections = 1000
max_requests = 1000
 
debug = False
daemon = False
pidfile = '/var/run/superdelivery.pid'
 
#user = 'nginx'
#group = 'nginx'
 
#errorlog = '/var/log/gunicorn/microblog-error.log'
#accesslog = '/var/log/gunicorn/microblog-access.log'
 
loglevel = 'info'
proc_name = 'superdelivery'

timeout = 600
graceful_timeout = 600
