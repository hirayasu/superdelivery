from .common import *


DEBUG = True

ALLOWED_HOSTS = ["*"]


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'makerdb',
        'USER': os.environ['dbuser'],
        'PASSWORD': os.environ['dbpass'],
    }
}



