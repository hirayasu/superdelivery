from .common import *
import os
import environ
from dotenv import load_dotenv
load_dotenv()
env = environ.Env()
env.read_env('.env')

# SECURITY WARNING: don't run with debug turned on in production!


DEBUG = False

ALLOWED_HOSTS = ["sunekka.biz"]

INSTALLED_APPS += (
    'gunicorn',
)


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'makerdb',
        'USER': os.environ['dbuser'],
        'PASSWORD': os.environ['dbpass'],
    }
}

