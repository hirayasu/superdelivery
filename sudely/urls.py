from django.urls import path, include
from . import views
from .views import ContactFormView, ContactResultView

app_name= 'sudely'

urlpatterns = [
    path('', views.home_view, name='home'),
    path("sudely/", views.GetData.as_view(), name="index"),
    path("netsea/", views.netsea.as_view(), name="netsea"),
    path("zakka/", views.zakka.as_view(), name="zakka"),
    path("logpass/", views.Formidpassview.as_view(), name="logpass"),
    path('login/', views.MyLoginView.as_view(), name="login"),
    path('logout/', views.MyLogoutView.as_view(), name="logout"),
    path('create/', views.UserCreateView.as_view(), name="create"),
    path("kawada/", views.kawada.as_view(), name="kawada"),
    path("kenko/", views.kenko.as_view(), name="kenko"),
    path("download/", views.downloader, name="downloader"),
    path("kanri/", views.kanri.as_view(), name="kanri"),
    path('contact/', ContactFormView.as_view(), name='contact_form'),
    path('contact/result/', ContactResultView.as_view(), name='contact_result'),
    path("log/", views.StatusView.as_view(), name="status"),
    path("exp/", views.expview.as_view(), name="exp"),
]
