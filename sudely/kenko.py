import requests
from bs4 import BeautifulSoup as bs
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import datetime as dt
import os
import re
import pandas as pd
from .rakuten import send_line_notify, send_line_notify2

def Get_Kenko(url, pages, kenkoid, kenkopass, username):
    if url.find("https://www1.kenko064.com/products/list?category_id") == -1:
        send_line_notify("kenko卸のURLを入力してください")
        send_line_notify2("kenko卸のURLを入力してください")
        return ("kenko卸のURLを入力してください")
    try:
        totalres = []
        for page in range(pages):
            if page != 0:
                browser.get(url)
                try:
                    browser.find_element_by_link_text("次へ").click()
                    url = browser.current_url
                except:
                    break
            res = requests.get(url)
            soup = bs(res.content, "html.parser")
            items = soup.select(".ec-shelfGrid__item")
            result = [{
                "url": item.select("a")[0].get("href"),
                "image": "https://www1.kenko064.com/" + item.select("a")[0].select("img")[0].get("src"),
                "title": item.select("a")[0].select(".product-list-kenko-name")[0].text,
            }
                for item in items
            ]

            if page == 0:
                options = Options()
                options.add_argument('--headless')
                options.add_argument('--no-sandbox')
                options.add_argument('--disable-dev-shm-usage')
                homeurl = "https://www1.kenko064.com/"
                browser = webdriver.Chrome(chrome_options=options)
                browser.get(homeurl)
                time.sleep(1)
                browser.implicitly_wait(10)

                try:
                    browser.find_elements_by_class_name("ec-headerNav__itemLink")[2].click()
                except:
                    send_line_notify("ログインできませんでした")
                    send_line_notify("ログインできませんでした")
                    return ("ログインエラー")

                logid = browser.find_element_by_name("login_email")
                logid.send_keys(kenkoid)
                logpass = browser.find_element_by_name("login_pass")
                logpass.send_keys(kenkopass)
                browser.find_elements_by_css_selector(".ec-blockBtn--cancel")[0].click()
                time.sleep(1)
                browser.implicitly_wait(10)

            for a in range(len(result)):
                browser.get(result[a]["url"])
                time.sleep(1)
                browser.implicitly_wait(1)
                result[a].update(
                    tanka=browser.find_elements_by_class_name("ec-price")[0].find_elements_by_css_selector(
                        ".ec-price__price.price02-custom")[0].text, \
                    jan=browser.find_elements_by_css_selector(".table-box-td.table-right-box-size")[
                        0].find_element_by_tag_name("span").text)

            totalres = totalres + result
        browser.close()

        results = [{
            "image": totalres[result1]["image"],
            "url": totalres[result1]["url"],
            "title": totalres[result1]["title"],
            "jan": totalres[result1]["jan"],
            "moq": "なし",
            "price": totalres[result1]["tanka"],
            "tanka": totalres[result1]["tanka"],
            "site": "Kenko卸",
            "author": username
        }
            for result1 in range(len(totalres))
            if totalres[result1]["url"] != ""
        ]

        return (results)
    except:
        return ("予期せぬエラーが起こりました。kenko卸をやり直してください。")