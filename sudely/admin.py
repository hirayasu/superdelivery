from django.contrib import admin
from .models import Formidpass
from .models import Request, Celerytask


myModels = [Formidpass, Request, Celerytask]
admin.site.register(myModels)
