from django.db import models
from django.contrib.auth import get_user_model

class Formidpass(models.Model):
    sudelyid = models.EmailField(blank=True)
    sudelypass = models.CharField(max_length=30, blank=True)
    netseaid = models.EmailField(blank=True)
    netseapass = models.CharField(max_length=30, blank=True)
    zakkaid = models.EmailField(blank=True)
    zakkapass = models.CharField(max_length=30, blank=True)
    kawadaid = models.CharField(max_length=30, blank=True)
    kawadapass = models.CharField(max_length=30, blank=True)
    kenkoid = models.EmailField(blank=True)
    kenkopass = models.CharField(max_length=30, blank=True)
    mwsseller = models.CharField(max_length=30, blank=True)
    authtoken = models.CharField(max_length=100, blank=True)
    rakuapi = models.CharField(max_length=100, blank=True)
    yahooapi = models.CharField(max_length=100, blank=True)
    author = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)

    def __str__(self):
        return str(self.author)

class Request(models.Model):
    image = models.TextField(blank=True, null=True)
    url = models.TextField(blank=True, null=True)
    title = models.TextField(blank=True, null=True)
    jan = models.TextField(blank=True, null=True)
    moq = models.TextField(blank=True, null=True)
    price = models.TextField(blank=True, null=True)
    tanka = models.TextField(blank=True, null=True)
    asin = models.TextField(blank=True, null=True)
    rank = models.TextField(blank=True, null=True)
    rakuprice = models.TextField(blank=True, null=True)
    rakuurl = models.TextField(blank=True, null=True)
    yahooprice = models.TextField(blank=True, null=True)
    cart = models.TextField(blank=True, null=True)
    low = models.TextField(blank=True, null=True)
    site = models.TextField(blank=True, null=True)
    taskid = models.TextField(blank=True, null=True)
    author = models.TextField(blank=True, null=True)

class Celerytask(models.Model):
    url = models.TextField(blank=True, null=True)
    site = models.TextField(blank=True, null=True)
    taskid = models.TextField(blank=True, null=True)
    taskstatus = models.TextField(blank=True, null=True)
    author = models.TextField(blank=True, null=True)
