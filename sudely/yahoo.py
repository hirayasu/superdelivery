import requests
import time
import datetime

def Get_yahoo(results, yahooapi):
    url = "https://shopping.yahooapis.jp/ShoppingWebService/V3/itemSearch"
    app_id = yahooapi

    for a in range(len(results)):

        if results[a]["jan"] != "-":
            payload = {
                'appid': app_id,
                'jan_code': results[a]["jan"],
                'sort': "+price",
            }
            r = requests.get(url, params=payload)
            res = r.json()
            time.sleep(1)

            try:
                results[a]["yahooprice"] = res["hits"][0]["price"]
            except:
                results[a]["yahooprice"] = "-"

        else:
            results[a]["yahooprice"] = "-"
            continue

    return results


if __name__ == "__main__":
    now = datetime.datetime.now()
