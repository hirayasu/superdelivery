from django import forms
from django.contrib.auth import forms as auth_forms
from .models import Formidpass
from django.contrib.auth.forms import AuthenticationForm
from django.conf import settings
from django.core.mail import BadHeaderError, send_mail
from django.http import HttpResponse

class RequestForm(forms.Form):
    url = forms.URLField(label="URL")
    pages = forms.IntegerField(initial=1, label="取得ページ数")
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['url'].widget.attrs["class"] = "url_field"

        choice1 = forms.fields.ChoiceField(
            choices=(
                ('rank', 'ランキング'),
                ('rank-', 'ランキング昇順'),
            ),
            required=True,
            widget=forms.widgets.Select
        )


class LoginForm(AuthenticationForm):
    '''ログインフォーム'''
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs['placeholder'] = field.label

class IdPassForm(forms.ModelForm):
    class Meta:
        model = Formidpass
        fields = ("sudelyid", "sudelypass", "netseaid", "netseapass", "zakkaid", "zakkapass",
                  "kawadaid", "kawadapass", "kenkoid", "kenkopass",
                  "mwsseller", "authtoken", "rakuapi", "yahooapi")

class ContactForm(forms.Form):
    name = forms.CharField(
        label='',
        max_length=100,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': "お名前",
        }),
    )
    email = forms.EmailField(
        label='',
        widget=forms.EmailInput(attrs={
            'class': 'form-control',
            'placeholder': "メールアドレス",
        }),
    )
    message = forms.CharField(
        label='',
        widget=forms.Textarea(attrs={
            'class': 'form-control',
            'placeholder': "お問い合わせ内容",
        }),
    )

    def send_email(self):
        subject = "お問い合わせ"
        message = self.cleaned_data['message']
        name = self.cleaned_data['name']
        email = self.cleaned_data['email']
        from_email = '{name} <{email}>'.format(name=name, email=email)
        recipient_list = [settings.EMAIL_HOST_USER]  # 受信者リスト
        try:
            send_mail(subject, message, from_email, recipient_list)
        except BadHeaderError:
            return HttpResponse("無効なヘッダが検出されました。")

