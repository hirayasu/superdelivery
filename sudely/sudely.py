import requests
from bs4 import BeautifulSoup as bs
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import re
import os
import pandas as pd
import datetime as dt
from .rakuten import send_line_notify

def Get_Sudely(url, pages, sudelyid, sudelypass, username):
#    url = "https://www.superdelivery.com/p/do/dpsl/112851/"
    if url.find("https://www.superdelivery.com/p/do/dpsl/") == -1:
        return("スーパーデリバリーのURLを入れてください")
    totalres = []
    try:
        for page in range(pages):
            if page != 0:
                browser.get(url)
                try:
                    browser.find_element_by_link_text("次へ").click()
                    url = browser.current_url
                except:
                    break
            res = requests.get(url)
            soup = bs(res.content, "html.parser")
            items = soup.select(".itembox-parts")
            result = [{
                "url": "https://www.superdelivery.com" + item.select(".item-name")[0].select("a")[0].get("href"),
                "image": "https:" + item.select(".item-img-box.wish-dvs-jdg.item-wish")[0].select("img")[0].get("src")
                }
                for item in items
            ]

            if page == 0:
                options = Options()
                options.add_argument('--headless')
                options.add_argument('--no-sandbox')
                options.add_argument('--disable-dev-shm-usage')
                homeurl = "https://www.superdelivery.com/"
                browser = webdriver.Chrome(chrome_options=options)
                browser.get(homeurl)
                time.sleep(1)
                browser.implicitly_wait(10)
                try:
                    browser.find_element_by_link_text("ログイン").click()
                except:
                    browser.close()

                    return ("ログイン情報をご確認くださ"
                            "い")
                time.sleep(1)
                browser.implicitly_wait(10)
                sendid = sudelyid
                sendpass = sudelypass
                login_id = browser.find_element_by_name("identification")
                login_id.send_keys(sendid)
                password = browser.find_element_by_name("password")
                password.send_keys(sendpass)
                time.sleep(1)
                if len(browser.find_elements_by_css_selector(".co-btn.co-btn-red.co-btn-s.co-btn-page")):
                    browser.find_element_by_xpath("//input[@value='ログイン']").click()
                browser.implicitly_wait(10)
                time.sleep(1)
                if len(browser.find_elements_by_css_selector(".log-btn")):
                    browser.close()
                    return("ログイン情報をご確認ください")


            for a in range(len(result)):
                browser.get(result[a]["url"])
                time.sleep(1)
                lenjan = len(browser.find_elements_by_css_selector(".co-fcgray.td-jan"))
                if lenjan == 0:
                    result[a] = [{"url": ""}]
                    continue
                tab = browser.find_element_by_css_selector(".set-list")
                result[a] = [
                    {
                        "image": result[a]["image"],
                        "url": result[a]["url"],
                        "title": browser.find_element_by_css_selector(".product-name").text,
                        "jan": re.sub("[\t\n]", "", tab.find_elements_by_css_selector(
                            ".co-fcgray.td-jan")[s].get_attribute("textContent")) if lenjan > s else "-",
                        "detail": tab.find_elements_by_css_selector(".border-rt.co-align-left.border-b.td-set-detail")[
                                      s].text \
                            [:tab.find_elements_by_css_selector(".border-rt.co-align-left.border-b.td-set-detail")[
                                  s].text.find(
                            "JAN") - 1] \
                            if browser.find_elements_by_css_selector(".border-rt.co-align-left.border-b.td-set-detail")[
                                   s].text.find("JAN") != -1 else \
                            browser.find_elements_by_css_selector(".border-rt.co-align-left.border-b.td-set-detail")[s].text \
                                [:browser.find_elements_by_css_selector(".border-rt.co-align-left.border-b.td-set-detail")[
                                      s].text \
                                      .find(
                                browser.find_elements_by_css_selector(".border-rt.co-align-left.border-b.td-set-detail")[s] \
                                    .text) + len((browser.find_elements_by_css_selector(
                                ".border-rt.co-align-left.border-b.td-set-detail")[s].text))],

                        "price": re.sub("[\t\n]", "",
                                        tab.find_elements_by_css_selector(".maker-wholesale-set-price.co-align-right")[
                                            s].get_attribute("textContent")),

                        "tanka": browser.find_elements_by_css_selector(
                                       ".border-r.co-align-right.co-pr5.co-pl5.co-pb5.td-price02")[s].get_attribute("textContent") \
                                if browser.find_elements_by_css_selector(
                                       ".border-r.co-align-right.co-pr5.co-pl5.co-pb5.td-price02")[s].get_attribute("textContent").find("¥") == \
                                   browser.find_elements_by_css_selector(
                                       ".border-r.co-align-right.co-pr5.co-pl5.co-pb5.td-price02")[s].get_attribute(
                                       "textContent").rfind("¥")
                                else browser.find_elements_by_css_selector(
                                       ".border-r.co-align-right.co-pr5.co-pl5.co-pb5.td-price02")[s].get_attribute(
                                       "textContent")[browser.find_elements_by_css_selector(
                                       ".border-r.co-align-right.co-pr5.co-pl5.co-pb5.td-price02")[s].get_attribute(
                                       "textContent").rfind("¥"):]
                    }
                    for s in range(lenjan)
                ]


                browser.implicitly_wait(10)
                time.sleep(1)
            totalres = totalres + result

        browser.close()

        results = [{
            "image": totalres[result1][result2]["image"],
            "url": totalres[result1][result2]["url"],
            "title": totalres[result1][result2]["title"],
            "jan": totalres[result1][result2]["jan"].replace("JAN：", ""),
            "moq": totalres[result1][result2]["detail"],
            "price": totalres[result1][result2]["price"],
            "tanka": totalres[result1][result2]["tanka"],
            ""
            "site": "スーパーデリバリー",
            "author": username,
            }
            for result1 in range(len(totalres))
            for result2 in range(len(totalres[result1]))
            if totalres[result1][0]["url"] != ""
         ]
        return (results)
    except:
        return ("予期せぬエラーが起こりました。Superdeliveryをやり直してください。")
