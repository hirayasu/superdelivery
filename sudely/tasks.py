from .zakka import Get_Zakka
from .netsea import Get_Netsea
from .sudely import Get_Sudely
from .kawada import Get_Kawada
from .kenko import Get_Kenko
from .mwsapi import Get_mws
from .rakuten import Get_rakuten
from .rakuten import send_line_notify, send_line_notify2
from .yahoo import Get_yahoo
from celery import shared_task
from sqlalchemy import create_engine
import pandas as pd

@shared_task
def starttask(url, pages, id, password, seller, access, rakuapi, yahooapi, username, sitename):
    if sitename == "スーパーデリバリー":
        send_line_notify(sitename+"の情報を取得します")
        # send_line_notify2(sitename+"の情報を取得します")
        result = Get_Sudely(url, pages, id, password, username)
    elif sitename == "NETSEA":
        send_line_notify(sitename+"の情報を取得します")
        # send_line_notify2(sitename+"の情報を取得します")
        result = Get_Netsea(url, pages, id, password, username)
    elif sitename == "ザッカネット":
        send_line_notify(sitename+"の情報を取得します")
        # send_line_notify2(sitename+"の情報を取得します")
        result = Get_Zakka(url, pages, id, password, username)
    elif sitename == "カワダ":
        send_line_notify(sitename+"の情報を取得します")
        # send_line_notify2(sitename+"の情報を取得します")
        result = Get_Kawada(url, pages, id, password, username)
    elif sitename == "Kenko卸":
        send_line_notify(sitename+"の情報を取得します")
        # send_line_notify2(sitename+"の情報を取得します")
        result = Get_Kenko(url, pages, id, password, username)

    if isinstance(result, str):
        send_line_notify(result)
        # send_line_notify2(result)
        return result

    if seller != "" and access != "":
        result = Get_mws(result, seller, access)
    if rakuapi != "":
        result = Get_rakuten(result, rakuapi)
    if yahooapi != "":
        result = Get_yahoo(result, yahooapi)
    df = pd.DataFrame(result)
    try:
        df = df.drop(["no"], axis=1)
    except:
        pass
    try:
        df = df.drop(["status"], axis=1)
    except:
        pass
    engine = create_engine('mysql://keeeepa1212:!Ju6td123@localhost/makerdb')
    df.to_sql('sudely_request', engine, if_exists='append', index=False)
    send_line_notify(sitename+"正常に終了しました。")
    # send_line_notify2(sitename+"正常に終了しました。")
    return result

