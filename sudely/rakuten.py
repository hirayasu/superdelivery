import requests
import time
import datetime
import environ
import os
from dotenv import load_dotenv

load_dotenv()
env = environ.Env()
env.read_env('.env')

def Get_rakuten(results, rakuapi):
    url = "https://app.rakuten.co.jp/services/api/IchibaItem/Search/20170706"
    app_id = rakuapi

    for a in range(len(results)):
        if results[a]["jan"] != "-":
            payload = {
                'applicationId': app_id,
                'hits': 30,
                'keyword': results[a]["jan"],
                'page': 1,
                'sort': "+itemPrice",
                'affiliateId': "15b5a09e.9d14f767.15b5a09f.ed613b59",
                'NGKeyword': "中古　レンタル".encode('utf-8')
            }
            r = requests.get(url, params=payload)

            res = r.json()
            time.sleep(1)

            try:
                results[a]["rakuprice"] = res["Items"][0]["Item"]["itemPrice"]
                results[a]["rakuurl"] = res["Items"][0]["Item"]["itemUrl"]
            except:
                results[a]["rakuprice"] = "-"
                results[a]["rakuurl"] = "-"

        else:
            results[a]["rakuprice"] = "-"
            results[a]["rakuurl"] = "-"
            continue

    return results


def send_line_notify(notification_message):

    line_notify_token = os.environ['notify']
    line_notify_api = 'https://notify-api.line.me/api/notify'
    headers = {'Authorization': f'Bearer {line_notify_token}'}
    data = {'message': f'{notification_message}'}
    requests.post(line_notify_api, headers = headers, data = data)

def send_line_notify2(notification_message):

    line_notify_token = os.environ['notify2']
    line_notify_api = 'https://notify-api.line.me/api/notify'
    headers = {'Authorization': f'Bearer {line_notify_token}'}
    data = {'message': f'{notification_message}'}
    requests.post(line_notify_api, headers = headers, data = data)

if __name__ == "__main__":
    now = datetime.datetime.now()