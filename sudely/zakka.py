import requests
from bs4 import BeautifulSoup as bs
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import datetime as dt
import os
import pandas as pd
from .rakuten import send_line_notify

def Get_Zakka(url, pages, zakkaid, zakkapass, username):

    if url.find("https://www.zakka.net/") == -1:
        return("ザッカネットのURLを入力してください")
    totalres = []
    try:
        for page in range(pages):
            if page != 0:
                browser.get(url)
                try:
                    browser.find_element_by_link_text("»").click()
                    url = browser.current_url
                except:
                    break
            res = requests.get(url)
            soup = bs(res.content, "html.parser")
            items = soup.select(".col-md-12.col-lg-3.block_item")
            result = [{
                "url": item.select("a")[0].get("href"),
                "image": item.select("img")[0].get("src"),
            }
                for item in items
            ]
            if page == 0:
                options = Options()
                options.add_argument('--headless')
                options.add_argument('--no-sandbox')
                options.add_argument('--disable-dev-shm-usage')
                homeurl = "https://www.zakka.net/"
                browser = webdriver.Chrome(chrome_options=options)
                browser.get(homeurl)
                time.sleep(1)
                browser.implicitly_wait(10)

                try:
                    browser.find_element_by_link_text("ログイン").click()
                except:
                    return("error01")
                time.sleep(1)
                browser.implicitly_wait(10)
                zakkaid = zakkaid
                zakkapass = zakkapass
                sendid = zakkaid
                sendpass = zakkapass
                logid = browser.find_element_by_name("f_email")
                logid.send_keys(sendid)
                logpass = browser.find_element_by_name("f_pass")
                logpass.send_keys(sendpass)
                browser.find_element_by_name("f_login").click()
                time.sleep(1)
                browser.implicitly_wait(10)
                if not "@" in zakkaid or zakkapass == "" or len(browser.find_elements_by_css_selector(".alert.alert-danger")):
                    browser.close()
                    return("ログインに失敗しました")

            for a in range(len(result)):
                browser.get(result[a]["url"])
                time.sleep(1)
                browser.implicitly_wait(1)
                tab = browser.find_elements_by_css_selector(".table.table-striped")
                if a == 0:
                    try:
                        tab[0].find_elements_by_tag_name("tr")[1].find_elements_by_tag_name("td")[3].find_element_by_tag_name("strong")
                    except:
                        return("取引申請をしてください")
                try:
                    ifjan = tab[0].find_elements_by_tag_name("tr")[1].find_elements_by_tag_name("td")[0].find_element_by_tag_name("small").text
                    ifstrong = tab[0].find_elements_by_tag_name("tr")[1].find_elements_by_tag_name("td")[3].find_element_by_tag_name("strong")
                except:
                    result[a] = [{"url": ""}]
                    continue
                if not "JAN" in ifjan:
                    result[a] = [{"url": ""}]
                    continue
                browser.implicitly_wait(10)
                time.sleep(1)

                result[a] = [
                    {
                        "image": result[a]["image"],
                        "url": result[a]["url"],
                        "title": browser.find_element_by_tag_name("h1").text,
                        "jan": tab[0].find_elements_by_tag_name("tr")[s + 1].find_elements_by_tag_name("td")[0].find_element_by_tag_name("small").text[6:len(tab[0].find_elements_by_tag_name("tr")[s + 1].find_elements_by_tag_name("td")[0].find_element_by_tag_name("small").text) - 1].replace("　", "").replace(" ", ""),
                        "detail": tab[0].find_elements_by_tag_name("tr")[s + 1].find_elements_by_tag_name("td")[0].text[0:tab[0].find_elements_by_tag_name("tr")[s + 1].find_elements_by_tag_name("td")[0].text.find("\n")],
                        "price": tab[0].find_elements_by_tag_name("tr")[s + 1].find_elements_by_tag_name("td")[3].find_element_by_tag_name("strong").text,
                        "tanka": tab[0].find_elements_by_tag_name("tr")[s + 1].find_elements_by_tag_name("td")[3].text[tab[0].find_elements_by_tag_name("tr")[s + 1].find_elements_by_tag_name("td")[3].text.find("\n") + 1:] if tab[0].find_elements_by_tag_name("tr")[s + 1].find_elements_by_tag_name("td")[3].text.find("\n") != -1 else "",
                    }
                    for s in range(len(tab[0].find_elements_by_tag_name("tr")) - 1)
                ]
                browser.implicitly_wait(10)
                time.sleep(1)
            totalres = totalres + result
        browser.close()

        results = [{
            "image": totalres[result1][result2]["image"],
            "url": totalres[result1][result2]["url"],
            "title": totalres[result1][result2]["title"],
            "jan": totalres[result1][result2]["jan"],
            "moq": totalres[result1][result2]["detail"],
            "price": totalres[result1][result2]["price"],
            "tanka": totalres[result1][result2]["tanka"],
            "site": "ザッカネット",
            "author": username
        }
            for result1 in range(len(totalres))
            for result2 in range(len(totalres[result1]))
            if totalres[result1][0]["url"] != ""
        ]
        return (results)
    except:
        return ("予期せぬエラーが起こりました。Zakkanetをやり直してください。")