from django.shortcuts import render, redirect
from django.views import generic
from .forms import RequestForm
from .forms import IdPassForm
from .forms import LoginForm
from .models import Formidpass, Request, Celerytask
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView,CreateView # 追記
from django.contrib.auth.forms import UserCreationForm  # 追記
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import redirect
from django_celery_results.models import TaskResult
from celery.result import ResultBase
from celery.result import AsyncResult
from celery.result import ResultSet
from django.urls import reverse_lazy # 追記
import datetime
import csv
import io
from django.http import HttpResponse
import pandas as pd
from .tasks import starttask
from django.urls import reverse_lazy
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from .forms import ContactForm
import os
import environ

env = environ.Env()
env.read_env('.env')


class Formidpassview(generic.FormView):
    template_name = "sudely/idpass.html"
    form_class = IdPassForm
    def form_valid(self, form):
        qryset = form.save(commit=False)
        qryset.author = self.request.user
        try:
            obj = Formidpass.objects.get(author=self.request.user)
            obj.delete()
            qryset.save()
        except:
            qryset.save()
        return redirect("/")

    def get(self, request):

        try:
            dbitem = Formidpass.objects.get(author=self.request.user)
            initial_dict = dict(sudelyid=dbitem.sudelyid, sudelypass=dbitem.sudelypass,\
                                netseaid=dbitem.netseaid, netseapass=dbitem.netseapass,\
                                zakkaid=dbitem.zakkaid, zakkapass=dbitem.zakkapass,
                                kawadaid=dbitem.kawadaid, kawadapass=dbitem.kawadapass,
                                kenkoid=dbitem.kenkoid, kenkopass=dbitem.kenkopass,
                                mwsseller=dbitem.mwsseller, authtoken=dbitem.authtoken,
                                rakuapi=dbitem.rakuapi, yahooapi=dbitem.yahooapi,
                                )

            form = IdPassForm(request.GET or None, initial=initial_dict)
        except:
            form = IdPassForm

        return render(request, "sudely/idpass.html", dict(form=form))

class GetData(LoginRequiredMixin, generic.FormView):
    template_name = "sudely/index.html"
    form_class = RequestForm

    def form_valid(self, form):
        now = datetime.datetime.now()
        url = form.cleaned_data["url"]
        username = str(self.request.user)
        urlpages = form.cleaned_data["pages"]
        dbitem = Formidpass.objects.get(author=self.request.user)
        sudelyid = dbitem.sudelyid
        sudelypass = dbitem.sudelypass
        seller = dbitem.mwsseller
        access = dbitem.authtoken
        rakuapi = dbitem.rakuapi
        yahooapi = dbitem.yahooapi
        sitename = "スーパーデリバリー"
        task = starttask.delay(url, urlpages, sudelyid, sudelypass, seller, access, rakuapi, yahooapi, username, sitename)
        task_id = task.id
        # タスクデータベースに保存
        taskstatus = AsyncResult(task_id).status
        savedb(username, sitename, url, task_id, taskstatus)
        result = Request.objects.filter(author=self.request.user, site=sitename).order_by("-id").all().distinct()
        paginator = Paginator(result, 100)
        p = self.request.GET.get('p', 1)
        try:
            ps = paginator.get_page(p)
        except PageNotAnInteger:
            ps = paginator.get_page(1)
        except EmptyPage:
            ps = paginator.get_page(1)
        context = {
            "tasks": task_id,
            "result": result,
            "form": self.form_class,
            "ps": ps,
            }

        now = datetime.datetime.now()
        return render(
            self.request,
            self.template_name,
            context
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        result = Request.objects.filter(author=self.request.user, site="スーパーデリバリー").order_by("-id").all().distinct()
        paginator = Paginator(result, 100)
        p = self.request.GET.get('p', 1)
        try:
            ps = paginator.get_page(p)
        except PageNotAnInteger:
            ps = paginator.get_page(1)
        except EmptyPage:
            ps = paginator.get_page(1)
        context = {
            "result": result,
            "form": self.form_class,
            "ps": ps,
        }
        return context

class netsea(LoginRequiredMixin, generic.FormView):
    template_name = "sudely/netsea.html"
    form_class = RequestForm

    def form_valid(self, form):
        now = datetime.datetime.now()
        url = form.cleaned_data["url"]
        dbitem = Formidpass.objects.get(author=self.request.user)
        netseaid = dbitem.netseaid
        netseapass = dbitem.netseapass
        username = str(self.request.user)
        seller = dbitem.mwsseller
        access = dbitem.authtoken
        rakuapi = dbitem.rakuapi
        yahooapi = dbitem.yahooapi
        pages = form.cleaned_data["pages"]
        task = starttask.delay(url, pages, netseaid, netseapass, seller, access, rakuapi, yahooapi, username, "NETSEA")
        task_id = task.id
        taskstatus = AsyncResult(task_id).status
        sitename = "NETSEA"
        savedb(username, sitename, url, task_id, taskstatus)
        result = Request.objects.filter(author=self.request.user, site="NETSEA").order_by("-id").all().distinct()
        paginator = Paginator(result, 100)
        p = self.request.GET.get('p', 1)
        try:
            ps = paginator.get_page(p)
        except PageNotAnInteger:
            ps = paginator.get_page(1)
        except EmptyPage:
            ps = paginator.get_page(1)
        context = {
            "tasks": task_id,
            "result": result,
            "form": self.form_class,
            "ps": ps,
        }

        now = datetime.datetime.now()
        return render(
            self.request,
            self.template_name,
            context
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        result = Request.objects.filter(author=self.request.user, site="NETSEA").order_by("-id").all().distinct()
        paginator = Paginator(result, 100)
        p = self.request.GET.get('p', 1)
        try:
            ps = paginator.get_page(p)
        except PageNotAnInteger:
            ps = paginator.get_page(1)
        except EmptyPage:
            ps = paginator.get_page(1)
        context = {
            "result": result,
            "form": self.form_class,
            "ps": ps,

        }
        return context

class zakka(LoginRequiredMixin, generic.FormView):
    template_name = "sudely/zakka.html"
    form_class = RequestForm

    def form_valid(self, form):
        now = datetime.datetime.now()
        url = form.cleaned_data["url"]
        username = str(self.request.user)
        dbitem = Formidpass.objects.get(author=self.request.user)
        zakkaid = dbitem.zakkaid
        zakkapass = dbitem.zakkapass
        pages = form.cleaned_data["pages"]
        seller = dbitem.mwsseller
        access = dbitem.authtoken
        rakuapi = dbitem.rakuapi
        yahooapi = dbitem.yahooapi
        task = starttask.delay(url, pages, zakkaid, zakkapass, seller, access, rakuapi, yahooapi, username, "ザッカネット")
        task_id = task.id
        taskstatus = AsyncResult(task_id).status
        sitename = "ザッカネット"
        savedb(username, sitename, url, task_id, taskstatus)
        result = Request.objects.filter(author=self.request.user, site="ザッカネット").order_by("-id").all().distinct()
        paginator = Paginator(result, 100)
        p = self.request.GET.get('p', 1)
        try:
            ps = paginator.get_page(p)
        except PageNotAnInteger:
            ps = paginator.get_page(1)
        except EmptyPage:
            ps = paginator.get_page(1)
        context = {
            "tasks": task_id,
            "result": result,
            "form": self.form_class,
            "ps": ps,
        }

        now = datetime.datetime.now()
        return render(
            self.request,
            self.template_name,
            context
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        result = Request.objects.filter(author=self.request.user, site="ザッカネット").order_by("-id").all().distinct()
        paginator = Paginator(result, 100)
        p = self.request.GET.get('p', 1)
        try:
            ps = paginator.get_page(p)
        except PageNotAnInteger:
            ps = paginator.get_page(1)
        except EmptyPage:
            ps = paginator.get_page(1)
        context = {
            "result": result,
            "form": self.form_class,
            "ps": ps,
        }
        return context


class kawada(LoginRequiredMixin, generic.FormView):
    template_name = "sudely/kawada.html"
    form_class = RequestForm

    def form_valid(self, form):
        now = datetime.datetime.now()
        url = form.cleaned_data["url"]
        pages = form.cleaned_data["pages"]
        username = str(self.request.user)
        dbitem = Formidpass.objects.get(author=self.request.user)
        kawadaid = dbitem.kawadaid
        kawadapass = dbitem.kawadapass
        seller = dbitem.mwsseller
        access = dbitem.authtoken
        rakuapi = dbitem.rakuapi
        yahooapi = dbitem.yahooapi
        task = starttask.delay(url, pages, kawadaid, kawadapass, seller, access, rakuapi, yahooapi, username, "カワダ")
        task_id = task.id
        taskstatus = AsyncResult(task_id).status
        sitename = "カワダ"
        savedb(username, sitename, url, task_id, taskstatus)
        result = Request.objects.filter(author=self.request.user, site="カワダ").order_by("-id").all().distinct()
        paginator = Paginator(result, 100)
        p = self.request.GET.get('p', 1)
        try:
            ps = paginator.get_page(p)
        except PageNotAnInteger:
            ps = paginator.get_page(1)
        except EmptyPage:
            ps = paginator.get_page(1)
        context = {
            "tasks": task_id,
            "result": result,
            "form": self.form_class,
            "ps": ps,
        }

        now = datetime.datetime.now()
        return render(
            self.request,
            self.template_name,
            context
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        result = Request.objects.filter(author=self.request.user, site="カワダ").order_by("-id").all().distinct()
        paginator = Paginator(result, 100)
        p = self.request.GET.get('p', 1)
        try:
            ps = paginator.get_page(p)
        except PageNotAnInteger:
            ps = paginator.get_page(1)
        except EmptyPage:
            ps = paginator.get_page(1)
        context = {
            "result": result,
            "form": self.form_class,
            "ps": ps,
        }
        return context

class kenko(LoginRequiredMixin, generic.FormView):
    template_name = "sudely/kenko.html"
    form_class = RequestForm

    def form_valid(self, form):
        now = datetime.datetime.now()
        url = form.cleaned_data["url"]
        pages = form.cleaned_data["pages"]
        username = str(self.request.user)
        dbitem = Formidpass.objects.get(author=self.request.user)
        kenkoid = dbitem.kenkoid
        kenkopass = dbitem.kenkopass
        seller = dbitem.mwsseller
        access = dbitem.authtoken
        rakuapi = dbitem.rakuapi
        yahooapi = dbitem.yahooapi
        task = starttask.delay(url, pages, kenkoid, kenkopass, seller, access, rakuapi, yahooapi, username, "Kenko卸")
        task_id = task.id
        taskstatus = AsyncResult(task_id).status
        sitename = "Kenko卸"
        savedb(username, sitename, url, task_id, taskstatus)
        result = Request.objects.filter(author=self.request.user, site="Kenko卸").order_by("-id").all().distinct()

        paginator = Paginator(result, 100)
        p = self.request.GET.get('p', 1)
        try:
            ps = paginator.get_page(p)
        except PageNotAnInteger:
            ps = paginator.get_page(1)
        except EmptyPage:
            ps = paginator.get_page(1)
        context = {
            "tasks": task_id,
            "result": result,
            "form": self.form_class,
            "ps": ps,
        }

        now = datetime.datetime.now()
        return render(
            self.request,
            self.template_name,
            context
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        result = Request.objects.filter(author=self.request.user, site="Kenko卸").order_by("-id").all().distinct()
        paginator = Paginator(result, 100)
        p = self.request.GET.get('p', 1)
        try:
            ps = paginator.get_page(p)
        except PageNotAnInteger:
            ps = paginator.get_page(1)
        except EmptyPage:
            ps = paginator.get_page(1)
        context = {
            "result": result,
            "form": self.form_class,
            "ps": ps,
        }
        return context

class kanri(generic.FormView):
    template_name = "kanri.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tasks = {
            "id": 100,
            "status": "test",
            "site": "testsudely"
        }
        context = {
            "task": tasks,
        }
        return context


class MyLogoutView(LogoutView):
    template_name = "sudely/login.html"

class MyLoginView(LoginView):
    form_class = LoginForm
    template_name = "sudely/login.html"
    succell_url = "/sudely/"


class UserCreateView(CreateView):
    form_class = UserCreationForm
    template_name = "sudely/create.html"
    success_url = reverse_lazy('sudely:sudely')

def home_view(request):
    return redirect('sudely/')

def downloader(request):
    response = HttpResponse(content_type='text/csv')
    filename = 'data.csv'  # ダウンロードするcsvファイル名
    response['Content-Disposition'] = 'attachment; filename={}'.format(filename)
    sio = io.StringIO()
    writer = csv.writer(sio)
    #writer = csv.writer(response)
    header = ["タイトル", '商品単価', "カート価格", "楽天価格", "ヤフー価格", "Amazonランキング", 'JAN', 'MOQ', 'URL']
    writer.writerow(header)
    order_list = Request.objects.filter(author=request.user).all().distinct()
    for data in order_list:
        writer.writerow([data.title, data.tanka, data.cart, data.rakuprice, data.yahooprice, data.rank, data.jan, data.moq, data.url])
    response.write(sio.getvalue().encode('utf_8_sig'))
    return response

class ContactFormView(FormView):
    template_name = 'sudely/contact_form.html'
    form_class = ContactForm
    success_url = reverse_lazy('sudely:contact_result')

    def form_valid(self, form):
        form.send_email()
        return super().form_valid(form)


class ContactResultView(TemplateView):
    template_name = 'sudely/contact_result.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['success'] = "お問い合わせは正常に送信されました。"
        return context




class StatusView(LoginRequiredMixin, generic.FormView):
    template_name = 'sudely/status.html'
    form_class = RequestForm

    def get_context_data(self, **kwargs):        
        results = Celerytask.objects.filter(author=self.request.user).all().distinct()
        for result in results:
            taskmodel = TaskResult.objects.filter(task_id=result.taskid)
            if len(taskmodel) > 0:
                qryset = Celerytask.objects.get(taskid=result.taskid)
                qryset.taskstatus = taskmodel[0].status
                qryset.save()

        context = super().get_context_data(**kwargs)
        context = {
            "result": results,
            "form": self.form_class,
        }
        return context

class expview(TemplateView):
    template_name = 'sudely/exp.html'


def savedb(username, sitename, url, task_id, taskstatus):
    # タスクデータベースに保存
    qryset = Celerytask()
    qryset.author = username
    qryset.site = sitename
    qryset.url = url
    qryset.taskid = task_id
    qryset.taskstatus = taskstatus
    qryset.save()
