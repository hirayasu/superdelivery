import requests
from bs4 import BeautifulSoup as bs
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import datetime as dt
import os
import re
import pandas as pd

def Get_Netsea(url, pages, netseaid, netseapass, username):
    #url = "https://www.netsea.jp/shop/354248"
    if url.find("https://www.netsea.jp/shop/") == -1:
        return("NETSEAのURLを入力してください")
    totalres = []

    try:
        for page in range(pages):
            if page != 0:
                browser.get(url)
                try:
                    browser.find_element_by_link_text("次のページ").click()
                    url = browser.current_url
                except:
                    break
            res = requests.get(url)
            soup = bs(res.content, "html.parser")
            items = soup.select(".showcaseType01")
            result = [{
                "url": item.select(".showcaseItemsImgBlock ")[0].select("a")[0].get("href"),
                "image": item.select(".showcaseItemsImgBlock")[0].select("img")[0].get("src") if "https://" in item.select(".showcaseItemsImgBlock")[0].select("img")[0].get("src")\
                    else item.select(".showcaseItemsImgBlock")[0].select("img")[0].get("data-src"),
            }
                for item in items
            ]

            if page == 0:
                options = Options()
                options.add_argument('--headless')
                options.add_argument('--no-sandbox')
                options.add_argument('--disable-dev-shm-usage')
                homeurl = "https://www.netsea.jp/"
                browser = webdriver.Chrome(chrome_options=options)
                browser.get(homeurl)
                time.sleep(1)
                browser.implicitly_wait(10)

                try:
                    browser.find_element_by_id("satori__popup_close").click()
                except:
                    pass

                try:
                    browser.find_element_by_link_text("ログイン").click()
                except:
                    return("loginエラー")

                time.sleep(1)
                browser.implicitly_wait(10)
                netseaid = netseaid
                netseapass = netseapass
                sendid = netseaid
                sendpass = netseapass
                logid = browser.find_element_by_name("login_id")
                logid.send_keys(sendid)
                logpass = browser.find_element_by_name("password")
                logpass.send_keys(sendpass)
                browser.find_element_by_name("submit").click()
                time.sleep(1)
                browser.implicitly_wait(10)

            for a in range(len(result)):
                browser.get(result[a]["url"])
                time.sleep(1)
                tabjan = browser.find_elements_by_css_selector(".tableType02")
                tabdet = browser.find_elements_by_css_selector(".tableType03")
                lenjan = len(tabjan[1].find_elements_by_tag_name("tr"))
                if a == 0:
                    order = tabjan[1].find_elements_by_tag_name("tr")[lenjan - 2].find_element_by_tag_name("td").text
                    if order == "取引申請が必要です":
    #                    result[a] = [{"url": ""}]
                        return("取引申請をしてください")


                jan = tabjan[1].find_elements_by_tag_name("tr")[lenjan - 1].find_element_by_tag_name("td").text

                if jan == "":
                    result[a] = [{"url": ""}]
                    continue
                title = browser.find_element_by_css_selector(".hdType01 ").text
                browser.implicitly_wait(1)
                time.sleep(1)

                result[a] = [
                    {
                        #            "setnum": browser.find_elements_by_css_selector(".set-num")[s * 2 - 1].text,
                        "image": result[a]["image"],
                        "url": result[a]["url"],
                        "title": title,
                        "jan": jan,
                        "detail": tabdet[1].find_elements_by_tag_name("tr")[s + 1].find_elements_by_tag_name("td")[2].text,
                        "price": tabdet[1].find_elements_by_tag_name("tr")[s + 1].find_elements_by_tag_name("td")[
                            6].find_elements_by_tag_name("p")[1].text,
                        "tanka": re.sub("[ \n]", "",
                                        tabdet[1].find_elements_by_tag_name("tr")[s + 1].find_elements_by_tag_name("td")[
                                            4].find_element_by_tag_name("p").get_attribute("textContent")),
                    }
                    for s in range(len(tabdet[1].find_elements_by_tag_name("tr")) - 1)

                ]
                browser.implicitly_wait(10)
                time.sleep(1)
            totalres = totalres + result
        browser.close()

        results = [{
            "image": totalres[result1][result2]["image"],
            "url": totalres[result1][result2]["url"],
            "title": totalres[result1][result2]["title"],
            "jan": totalres[result1][result2]["jan"],
            "moq": totalres[result1][result2]["detail"],
            "price": totalres[result1][result2]["price"],
            "tanka": totalres[result1][result2]["tanka"],
            "site": "NETSEA",
            "author": username
        }
            for result1 in range(len(totalres))
            for result2 in range(len(totalres[result1]))
            if totalres[result1][0]["url"] != ""
        ]

        return (results)
    except:
        return("予期せぬエラーが起こりました。netseaをやり直してください。")
