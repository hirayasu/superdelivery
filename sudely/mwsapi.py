import base64
import datetime
import hashlib
import hmac
import urllib.parse
import requests
import six
import time
from bs4 import BeautifulSoup
import environ

from .models import Formidpass
from .rakuten import send_line_notify
import webbrowser

from dotenv import load_dotenv

load_dotenv()


env = environ.Env()
env.read_env('.env')

DOMAIN = 'mws.amazonservices.jp'
ENDPOINT = '/Products/2011-10-01'


def PostMWS(q, AMAZON_CREDENTIAL):
    timestamp = datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')

    data = {
        'AWSAccessKeyId': AMAZON_CREDENTIAL['ACCESS_KEY_ID'],
        'MarketplaceId': 'A1VC38T7YXB528',
        'MWSAuthToken': AMAZON_CREDENTIAL['AUTH_TOKEN'],
        'SellerId': AMAZON_CREDENTIAL['SELLER_ID'],
        'SignatureMethod': 'HmacSHA256',
        'SignatureVersion': '2',
        'Timestamp': timestamp,
    }

    data.update(q)
    query_string = urllib.parse.urlencode(sorted(data.items()))
    canonical = "{}\n{}\n{}\n{}".format(
        'POST', DOMAIN, ENDPOINT, query_string
    )
    h = hmac.new(
        six.b(AMAZON_CREDENTIAL['ACCESS_SECRET']),
        six.b(canonical), hashlib.sha256
    )
    signature = urllib.parse.quote(base64.b64encode(h.digest()), safe='')
    url = 'https://{}{}?{}&Signature={}'.format(
        DOMAIN, ENDPOINT, query_string, signature)
    res = requests.post(url)
    return res


def GetMatchingProductForId(results, AMAZON_CREDENTIAL):
    count = -(-len(results) // 5)
    asinlist = []
    result = []

    for b in range(count):
        s = b * 5
        q = {
            'Action': 'GetMatchingProductForId',
            'MarketplaceId': 'A1VC38T7YXB528',
            'Version': '2011-10-01',
            'IdType': 'JAN',
        }

        p = [
            {
                "IdList.Id." + str(i + 1): results[s + i]["jan"]
            }
            for i in range(5)
            if s + i < len(results)
        ]

        for listp in p:
            q.update(listp)
        res = PostMWS(q, AMAZON_CREDENTIAL)
        soup = BeautifulSoup(res.content.decode(), "lxml")
        items = soup.find_all('getmatchingproductforidresult')
        time.sleep(1)
        for item in items:
            if item.get("status") == "Success":
                for a in range(5):
                    if s + a < len(results):
                        if results[s + a]["jan"] == item.get("id"):
                            try:
                                results[s + a]["asin"] = item.select("asin")[0].text
                                results[s + a]["status"] = item.get("status")
                                results[s + a]["rank"] = item.select("salesrankings")[0].select("salesrank")[0].select("rank")[0].text
                            except:
                                results[s + a]["asin"] = "-"
                                results[s + a]["rank"] = "-"
                                results[s + a]["status"] = "-"

                        elif results[s + a]["jan"] == "-":
                            results[s + a]["asin"] = "-"
                            results[s + a]["rank"] = "-"
                            results[s + a]["status"] = item.get("status")
                    else:
                        break
            else:
                for a in range(5):
                    if s + a < len(results):
                        if results[s + a]["jan"] == item.get("id"):
                            results[s + a]["asin"] = "-"
                            results[s + a]["rank"] = "-"
                            results[s + a]["status"] = item.get("status")
                            results[s + a]["no"] = (s + a)
                    else:
                        break

    return results


def GetLowestOfferListingsForASIN(results, AMAZON_CREDENTIAL):
    try:
        asinlist = [item["asin"] for item in results if item["asin"] != "-"]
    except KeyError:
        print("error:ASIN")
        exit()

    asinlist = list(set(asinlist))

    count = -(-len(asinlist) // 5)
    for b in range(count):
        s = b * 5
        q = {
            'Action': 'GetLowestOfferListingsForASIN',
            'MarketplaceId': 'A1VC38T7YXB528',
            'Version': '2011-10-01',
            'ItemCondition': "New",
            'ExcludeMe': True
        }

        p = [
            {
                "ASINList.ASIN." + str(i + 1): asinlist[s + i]
            }
            for i in range(5)
            if s + i < len(asinlist)
        ]

        for listp in p:
            q.update(listp)
        res = PostMWS(q, AMAZON_CREDENTIAL)
        soup = BeautifulSoup(res.content.decode(), "lxml")
        items = soup.find_all('getlowestofferlistingsforasinresult')
        time.sleep(1)
        for item in items:
            for a in range(len(results)):
                if item.get("status") == "Success":
                    if item.select("lowestofferlistings")[0].text != "":
                        if item.get("asin") == results[a]["asin"]:
                            results[a]["low"] = round(float(item.select("LowestOfferListing")[0].select("Price")[0].select("LandedPrice")[0].find("amount").text))
                    else:
                        continue
                else:
                    try:
                        if results[a]["low"] == "":
                            results[a]["low"] == "-"
                    except:
                        results[a]["low"] == "-"


    return results


def GetCompetitivePricingForASIN(results, AMAZON_CREDENTIAL):
    asinlist = [item["asin"] for item in results if item["asin"] != "-"]
    asinlist = list(set(asinlist))

    count = -(-len(asinlist) // 5)

    for b in range(count):
        s = b * 5
        q = {
            'Action': 'GetCompetitivePricingForASIN',
            'MarketplaceId': 'A1VC38T7YXB528',
            'Version': '2011-10-01',
            'ExcludeMe': True
        }

        p = [
            {
                "ASINList.ASIN." + str(i + 1): asinlist[s + i]
            }
            for i in range(5)
            if s + i < len(asinlist)
        ]
        for listp in p:
            q.update(listp)
        res = PostMWS(q, AMAZON_CREDENTIAL)
        soup = BeautifulSoup(res.content.decode(), "lxml")
        items = soup.find_all('getcompetitivepricingforasinresult')
        time.sleep(1)

        for item in items:
            """
            if item.get("status") == "Success":
                for a in range(len(results)):
                    if item.select("competitiveprices")[0].text != "":
                        if item.get("asin") == results[a]["asin"]:
                            results[a]["cart"] = round(float(item.select("competitiveprices")[0].select("Price")[0].select("LandedPrice")[0].find("amount").text))
                    else:
                        continue
            """
            for a in range(len(results)):
                if item.get("status") == "Success":
                    if item.select("competitiveprices")[0].text != "":
                        if item.get("asin") == results[a]["asin"]:
                            results[a]["cart"] = round(float(item.select("competitiveprices")[0].select("Price")[0].select("LandedPrice")[0].find("amount").text))
                    else:
                        continue
                else:
                    try:
                        if results[a]["cart"] == "":
                            results[a]["cart"] == "-"
                    except:
                        results[a]["cart"] == "-"




    return results


def Get_mws(results, seller, access):
    AMAZON_CREDENTIAL = {
        'SELLER_ID': seller,
        'ACCESS_KEY_ID': "AKIAIUHPXHVHAA6VUFRA",
        'AUTH_TOKEN': access,
        'ACCESS_SECRET': "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"
    }

    results = GetMatchingProductForId(results, AMAZON_CREDENTIAL)
    results = GetLowestOfferListingsForASIN(results, AMAZON_CREDENTIAL)
    results = GetCompetitivePricingForASIN(results, AMAZON_CREDENTIAL)
    return results
