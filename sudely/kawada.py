import requests
from bs4 import BeautifulSoup as bs
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import datetime as dt
import os
import re
import pandas as pd
from .rakuten import send_line_notify,send_line_notify2


def Get_Kawada(url, pages, kawadaid, kawadapass, username):
    if url.find("https://ganguoroshi.jp/category/") == -1:
        send_line_notify("カワダオンラインのURLを入力してください")
        send_line_notify2("カワダオンラインのURLを入力してください")
        return ("カワダオンラインのURLを入力してください")
    try:
        totalres = []
        for page in range(pages):
            if page != 0:
                browser.get(url)
                try:
                    browser.find_element_by_link_text("次ページを表示").click()
                    url = browser.current_url
                except:
                    break
            if page == 0:
                options = Options()
                options.add_argument('--headless')
                options.add_argument('--no-sandbox')
                options.add_argument('--disable-dev-shm-usage')
                homeurl ="https://ganguoroshi.jp/"
                browser = webdriver.Chrome(chrome_options=options)
                browser.get(homeurl)
                time.sleep(1)
                browser.implicitly_wait(10)

                try:
                    browser.find_element_by_class_name("login").click()
                except:
                    send_line_notify("ログインできませんでした")
                    send_line_notify2("ログインできませんでした")
                    return("ログインエラー")

                time.sleep(1)
                browser.implicitly_wait(10)
                logid = browser.find_element_by_name("LOGINID")
                logid.send_keys(kawadaid)
                logpass = browser.find_element_by_name("PASSWORD")
                logpass.send_keys(kawadapass)
                browser.find_elements_by_css_selector(".buttonarea")[0].click()
                time.sleep(1)
                browser.implicitly_wait(10)

            browser.get(url)
            result = [
                {
                    "url": item.find_element_by_class_name("itemname").find_element_by_tag_name("a").get_attribute("href"),
                    "image": item.find_elements_by_tag_name("a")[0].find_elements_by_tag_name("img")[0].get_attribute("src"),
                    "jan": item.find_elements_by_tag_name("h3")[0].text,
                    "title": item.find_element_by_class_name("itemname").find_element_by_tag_name("a").text,
                    "price":item.find_element_by_class_name("price").find_element_by_tag_name("span").text,
            }
                for item in browser.find_elements_by_class_name("box")
            ]

            totalres = totalres + result
        browser.close()
    #    return (totalres)

        results = [{
            "image": totalres[result1]["image"],
            "url": totalres[result1]["url"],
            "title": totalres[result1]["title"],
            "jan": totalres[result1]["jan"],
            "moq": "なし",
            "price": totalres[result1]["price"],
            "tanka": totalres[result1]["price"],
            "site": "カワダ",
            "author": username
        }
            for result1 in range(len(totalres))
            if totalres[result1]["url"] != ""
        ]

        return (results)
    except:
        return ("予期せぬエラーが起こりました。カワダオンラインをやり直してください。")
